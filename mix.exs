defmodule Cmder.MixProject do
  use Mix.Project

  @version "0.0.4"
  @source_url "https://gitlab.com/schutm/cmder"

  def project do
    [
      app: :cmder,
      version: @version,
      elixir: "~> 1.10",
      deps: deps(),
      description: "Mix tasks for invoking cmder with zombie prevention",
      package: [
        links: %{
          "GitLab" => @source_url
        },
        licenses: ["MIT"]
      ],
      docs: [
        main: "Cmder",
        source_url: @source_url,
        source_ref: "v#{@version}",
        extras: ["CHANGELOG.md"]
      ]
    ]
  end

  def application do
    [
      # inets/ssl may be used by Mix tasks but we should not impose them.
      extra_applications: [:logger],
      mod: {Cmder, []},
      env: [default: []]
    ]
  end

  defp deps do
    [
      {:ex_doc, ">= 0.0.0", only: :dev, runtime: false}
    ]
  end
end
