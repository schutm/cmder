# CHANGELOG

## v0.0.4 (2021-12-03)
  * BREAKING CHANGE: Add possibility to work without profiles

## v0.0.3 (2021-11-16)
  * Improve description (for usage on hex.pm)

## v0.0.2 (2021-11-16)

  * Add zombie process detection (on request)

## v0.0.1 (2021-11-15)

  * First release
