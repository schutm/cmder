defmodule Mix.Tasks.Cmder do
  @moduledoc """
  Invokes cmder with the given args.

  Usage:

      $ mix cmder TASK_OPTIONS PROFILE COMMAND_ARGS

  Example:

      $ mix cmder tailwindcss --watch

  Note the arguments given to this task will be appended
  to any configured arguments.

  ## Options

  Note flags to control this Mix task must be given before the
  profile:

      $ mix cmder --runtime-config tailwindcss

  """

  @shortdoc "Invokes cmder with the profile and args"

  use Mix.Task

  @impl true
  def run(args) do
    switches = [runtime_config: :boolean]
    {opts, remaining_args} = OptionParser.parse_head!(args, switches: switches)

    if opts[:runtime_config] do
      Mix.Task.run("app.config")
    else
      Application.ensure_all_started(:cmder)
    end

    Mix.Task.reenable("cmder")
    do_run(remaining_args)
  end

  defp do_run([profile | args] = all) do
    case Cmder.profile(String.to_atom(profile), args) do
      0 -> :ok
      status -> Mix.raise("`mix cmder #{Enum.join(all, " ")}` exited with #{status}")
    end
  end

  defp do_run([]) do
    Mix.raise("`mix cmder` expects the profile as argument")
  end
end
